﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class frmGato : Form
    {
        private int turno;
        private int[,] tablero;
        private readonly Image[] imagenes = {Properties.Resources.circulo, Properties.Resources.equis};
        private readonly Dictionary<string, int> parseButtons;
        public frmGato()
        {
            InitializeComponent();
            parseButtons = new Dictionary<string, int>();
            parseButtons["12"] = 0; parseButtons["88"] = 1; parseButtons["164"] = 2;

            turno = 0;
            tablero = new int[,]
            {
                {-1, -1, -1},
                {-1, -1, -1},
                {-1, -1, -1}
            };
        }

        private void buttonClick(object sender, EventArgs e)
        {
            Button a = (Button)sender;
            a.BackgroundImageLayout = ImageLayout.Zoom;
            clickHandling(parseButtons[a.Location.X.ToString()], parseButtons[a.Location.Y.ToString()], a);
        }

        private void clickHandling(int x, int y, Button sender)
        {
            if(tablero[x,y] == -1)
            {
                tablero[x, y] = turno;
                sender.BackgroundImage = this.imagenes[turno];

                if (juegoTerminado())
                {
                    limpiarTablero();
                }
                else
                {
                    if (turno == 0)
                    {
                        turno = 1;
                    }
                    else
                    {
                        turno = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("Este espacio ya ha sido utilizado.");
            }
        }

        private bool juegoTerminado()
        {
            if (hayLinea())
            {
                MessageBox.Show("Gánó el jugador " + (turno+1).ToString());
                return true;
            }
            else
            {
                bool emptySpace = false;
                foreach(int space in tablero)
                {
                    if (space == -1) emptySpace = true;
                }
                if (!emptySpace)
                {
                    MessageBox.Show("Ya no hay más espacios en el tablero.");
                    return true;
                }
                return false;
            }
        }

        private bool hayLinea()
        {
            if (tablero[1, 1] != -1 && tablero[1, 1] == tablero[0, 0] && tablero[2, 2] == tablero[1, 1])
            {
                /* O - -
                 * - O -
                 * - - O*/
                return true;
            }
            else if (tablero[1, 1] != -1 && tablero[1, 1] == tablero[2, 0] && tablero[0, 2] == tablero[1, 1])
            {
                /* - - O
                 * - O -
                 * O - -*/
                return true;
            }
            else if(tablero[0, 0] != -1 && tablero[0, 1] == tablero[0, 0] && tablero[0, 2] == tablero[0, 0])
            {
                /* O - -
                 * O - -
                 * O - -*/
                return true;
            }
            else if (tablero[1, 0] != -1 && tablero[1, 0] == tablero[1, 1] && tablero[1, 0] == tablero[1, 2])
            {
                /* - O -
                 * - O -
                 * - O -*/
                return true;
            }
            else if (tablero[2, 0] != -1 && tablero[2, 0] == tablero[2, 1] && tablero[2, 0] == tablero[2, 2])
            {
                /* - - O
                 * - - O
                 * - - O*/
                return true;
            }
            else if (tablero[0, 0] != -1 && tablero[1, 0] == tablero[0, 0] && tablero[2, 0] == tablero[0, 0])
            {
                /* O O O
                 * - - -
                 * - - -*/
                return true;
            }
            else if (tablero[0, 1] != -1 && tablero[0, 1] == tablero[1, 1] && tablero[0, 1] == tablero[2, 1])
            {
                /* - - -
                 * O O O
                 * - - -*/
                return true;
            }
            else if (tablero[0, 2] != -1 && tablero[0, 2] == tablero[1, 2] && tablero[0, 2] == tablero[2, 2])
            {
                /* - - -
                 * - - -
                 * O O O*/
                return true;
            }
            return false;
        }

        private void limpiarTablero()
        {
            turno = 0;
            tablero = null;
            tablero = new int[,]
            {
                {-1, -1, -1},
                {-1, -1, -1},
                {-1, -1, -1}
            };
            button1.BackgroundImage = null;
            button2.BackgroundImage = null;
            button3.BackgroundImage = null;
            button4.BackgroundImage = null;
            button5.BackgroundImage = null;
            button6.BackgroundImage = null;
            button7.BackgroundImage = null;
            button8.BackgroundImage = null;
            button9.BackgroundImage = null;
        }
    }
}
